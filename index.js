const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000;
const app = express();
const cors = require('cors');

let userRoutes = require("./routes/userRoutes");
let productRoutes = require("./routes/productRoutes");

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

mongoose.connect("mongodb+srv://kirbycpunay:kirby0421@cluster0.outkb.mongodb.net/ecommerce?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(()=>console.log(`Connected to Database`))
.catch( (error)=> console.log(error))

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(PORT, () => console.log(`Server functions at port ${PORT}`));