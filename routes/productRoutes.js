const express = require('express');
const router = express.Router();
let auth = require('./../auth');
const productController = require('./../controllers/productControllers');

//add product
router.post('/addProduct', auth.verify, (req, res) => {

	productController.addProduct(req.body).then( result => res.send(result))
})

//retrieve all products
router.get("/allProduct", (req, res) => {

	productController.allProduct().then( result => res.send(result))
})

//retrieve single product
router.get('/:productId', (req, res) => {

	productController.getSingleProduct(req.params).then( result => res.send(result))
})

//update course
router.put('/:productId/edit', auth.verify, (req, res) => {

	productController.editProduct(req.params.productId, req.body).then( result => res.send(result))
})

//archive product
router.put('/:productId/archive', auth.verify, (req, res) => {

	productController.archiveProduct(req.params.productId).then( result => res.send(result))
})

//unarchive product
router.put('/:productId/unarchive', auth.verify, (req, res) => {

	productController.unarchiveProduct(req.params.productId).then( result => res.send(result))
})



module.exports = router;