const User = require("./../models/User");
const Product = require('./../models/Products');
const bcrypt = require('bcrypt');
const auth = require('./../auth');

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){ 
			return true
		} else {
			return false
		}
	})
}

module.exports.register = (reqBody) => {

	let user = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return user.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return "registered"
		}
	})
}

module.exports.login = (reqBody) => {
	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
} 

module.exports.setAsAdmin = (params) => {

	let setAsAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(params, setAsAdmin, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

module.exports.removeAsAdmin = (params) => {

	let removeAsAdmin = {
		isAdmin: false
	}

	return User.findByIdAndUpdate(params, removeAsAdmin, {new: true})
	.then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

module.exports.checkout = async (data) => {

	const userOrderProds = await User.findById(data.userId).then( user => {
		user.orders.push({productId: data.productId, totalAmount: data.totalAmount})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return user
			}
		})
	})

	const userProducts = await Product.findById(data.productId).then( product => {
		product.purchasers.push({userId: data.userId, totalAmount: data.totalAmount})

		return product.save().then( (product, error) => {
			if(error){
				return false
			} else {
				return product
			}
		})
	})


	if(userOrderProds && userProducts){
		return "success"
	} else {
		return false
	}
}

module.exports.allOrders = () => {
	
	return User.find({isAdmin:false}, {_id: 0, email: 0, password: 0, isAdmin: 0} ).then( (result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

module.exports.myOrders = (params) => {
	
	return User.findById(params).then( (result) => {

		return result
	})
}